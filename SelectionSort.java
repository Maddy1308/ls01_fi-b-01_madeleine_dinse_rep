import java.util.Random;
import java.util.Arrays;

public class SelectionSort {

	public static void main(String[] args) {
        int[] arr = new int[10];
        Random random = new Random();
		for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(100);
        }
        System.out.println("Unsortiertes Array: " + Arrays.toString(arr));
        selectionSort(arr);
        ausgabe(arr);
        }
    
    public static int[] selectionSort(int[] arr){
        for(int i = 0; i < arr.length - 1; i++){
            for(int j = i+1; j < arr.length; j++){
                if(arr[j] < arr[i]){
                    int tmp = arr[i];
                    arr[i] = arr[j];
                    arr[j] = tmp;
                }
            }

        }
        return arr;
    }
    public static void ausgabe(int[] arr) {
		System.out.println("Sortiertes Array : " + Arrays.toString(arr));

	}
}