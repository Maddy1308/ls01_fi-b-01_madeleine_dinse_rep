import java.util.Scanner;

public class UriFormel {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.print("Bitte geben Sie an ob sie R, U oder I berechnen wollen:");

		String a = sc.next();
		
		while (a != "R" || a != "U" || a != "I" || a != "u" || a != "i" || a != "r") {
			System.out.println("Ung�ltige Eingabe, bitte versuchen Sei es erneut!");
			a = sc.next();
		}
		
		double r; 
		double u; 
		double i;
		double ergebnis = 0.0;
		 
		switch (a){
			case "R": 
			case "r":
				System.out.print("Bitte geben Sie erst U ein und dann I (getrennt mit einem Enter!)");
				u = sc.nextDouble();
				i = sc.nextDouble();
				ergebnis = u/i;
				break;
			case "U":
			case "u":
				System.out.print("Bitte geben Sie erst R ein und dann I (getrennt mit einem Enter!)");
				r = sc.nextDouble();
				i = sc.nextDouble();
				ergebnis = r*i;
				break;
			case "I":
			case "i":
				System.out.print("Bitte geben Sie erst U ein und dann R (getrennt mit einem Enter!)");
				u = sc.nextDouble();
				r = sc.nextDouble();
				ergebnis = u/r;
				break;
			//default:
			//	ergebnis = 0.0;
		}
		System.out.println("Ihr Ergebnis betr�gt:" + ergebnis);
	}
}
