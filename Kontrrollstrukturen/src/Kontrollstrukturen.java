import java.util.Scanner;

public class Kontrollstrukturen {

	public static void main(String[] args) {
		int jahreszahl;
		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie eine vierstellige Jahreszahl an (muss gr��er als 0000 sein):");
		jahreszahl = sc.nextInt();
		while (jahreszahl < 0001 && jahreszahl > 9999) {
			System.out.println("Ung�ltige Eingabe. Bitte geben Sie die Jahreszahl erneut ein:");
			jahreszahl = sc.nextInt();
		}
		if (jahreszahl < 1582 && jahreszahl > 0 ) {
			System.out.println(regelEins(jahreszahl));
		}
		else if(jahreszahl >= 1582) {
			System.out.println(alles(jahreszahl));
		}
		
	}

	public static String alles(int jahreszahl) {
		if(jahreszahl % 4 == 0 && jahreszahl %100 == 0 && jahreszahl %400 == 0) {
			return "Das Jahr: " + jahreszahl + " ist ein Schalltjahr.";
		}
		else if (jahreszahl % 4 == 0 && jahreszahl %100 == 0) {
			return "Das Jahr: " + jahreszahl + " ist kein Schaltjahr.";
		}
		else if(jahreszahl % 4 == 0) {
			return "Das Jahr: " + jahreszahl + " ist ein Schaltjahr.";
		}
		else {
			return "Das Jahr " + jahreszahl + " ist kein Schaltjahr.";
		}

	}
	
	public static String regelEins(int jahreszahl) {
		if (jahreszahl %4 == 0) {
			return "Das Jahr " + jahreszahl + " ist ein Schaltjahr.";
		}
		else if (jahreszahl %4 != 0) {
			return "Das Jahr " + jahreszahl + " ist kein Schaltjahr.";
		}
		else {
			return "Bei der �berpr�fung des Jahres " + jahreszahl + " ist etwas schief gelaufen";
		}
	}
}
