import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

// Benutzer wird gefragt, wie viele Zahlen sollen per Zufall zwischen 0 und 99 erzeugt werden.
// Mit dem Wert der Eingabe des Benutzers ein Array mit Integer Werte dieser Länge Deklarieren und mit zufällige erzeugte Werte belegen.
// Mittelwert aus den Werten des Arrays berechnen
// Anschließend den Mittelwert Ausgeben.
// Zwischendurch sollen Sie auch den Array mit den erzeugten Werten ausgeben.

public class MittelwertOhneArray {

	public static void main(String[] args) {

		double m;
		int[] arr;
		programmhinweis();
		// (E) "Eingabe"
		arr = erzeugeArray("Bitte geben Sie die Anzahl der zufällig zu generierenden Zahlen an: ");
		// (V) Verarbeitung
		m = berechneMittelwert(arr);

		// (A) Ausgabe
		ausgabe(arr, m);

	}

	// Methode Programmhinweis
	public static void programmhinweis() {
		System.out.println(
				"Dieses Programm berechnet den arithmetischen Mittelwert eines Arrays mit einer übergebenen Länge.");
	}

	// Methode Eingabe
	public static int[] erzeugeArray(String text) {
		Scanner scan = new Scanner(System.in);
		System.out.print(text);
		int zahl = scan.nextInt();
		int[] zahlenArray = new int[zahl];
		scan.close();
		Random random = new Random();
		for (int i = 0; i < zahlenArray.length; i++) {
			zahlenArray[i] = random.nextInt(100);
		}
		return zahlenArray;
	}

	//Methode Berechnung
	public static double berechneMittelwert(int[] zahlArray) {
		double mittelwert = 0.0;
		double zusammen = 0;
		for (int i = 0; i < zahlArray.length; i++) {
			zusammen += zahlArray[i];
		}
		mittelwert = zusammen / zahlArray.length;
		return mittelwert;
	}

	// Methode Ausgabe
	public static void ausgabe(int[] arr, double m) {
		System.out.println("Array : " + Arrays.toString(arr));
		System.out.printf("Der Mittelwert vom Array ist %.2f\n", m);
	}
}