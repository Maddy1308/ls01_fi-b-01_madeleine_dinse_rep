import java.util.Scanner;

public class Sortieren {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie drei Zeichen ohne Leerzeichen nacheinander an:");

		String readLine = sc.nextLine();
		char a = readLine.charAt(0);
		char b = readLine.charAt(1);
		char c = readLine.charAt(2);
		
		if (a < b) {
			if (b < c) {
				System.out.println("Die Reihenfolge der Zeichenkette ist: " + a +b+ c);
			}
			else {
				System.out.println("Die Reihenfolge der Zeichenkette ist: " + a +c+ b);
			}
		}
		else if ( a > b){
			if (a < c) {
				System.out.println("Die Reihenfolge der Zeichenkette ist: " + b +a+ c);
			}
			else {
				System.out.println("Die Reihenfolge der Zeichenkette ist: " + b +c+ a);
			}
		}
		else if (c < a) {
			if (a < b) {
				System.out.println("Die Reihenfolge der Zeichenkette ist: " + c + a + b);
			}
			else {
				System.out.println("Die Reihenfolge der Zeichenkette ist: " + c +b + a);
			}
		}
	}

}
