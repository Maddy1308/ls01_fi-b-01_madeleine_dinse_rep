package Angestellter;

public class Angestellter {

	 private String name;
	 private double gehalt;
	 private String vorname;
	 //TODO: 3. Fuegen Sie in der Klasse 'Angestellter' das Attribut 'vorname' hinzu und implementieren Sie die entsprechenden get- und set-Methoden.
	 //TODO: 4. Implementieren Sie einen Konstruktor, der alle Attribute initialisiert.
	 public Angestellter() {
		name = "Müller";
		gehalt = 5000;
		vorname = "Hans";
	}
	 //TODO: 5. Implementieren Sie einen Konstruktor, der den Namen und den Vornamen initialisiert.
	 public Angestellter(String vorname, String name) {
		 this();
		 this.name = name;
		 this.vorname = vorname;
	 }
	 
	 public Angestellter(String vorname, String name, double gehalt) {
		 this(vorname, name);
		 this.gehalt = gehalt;	 
	 }
	 
	 public void setName(String name) {
	    this.name = name;
	 }
	 
	 public String getName() {
	    return this.name;
	 }
	public double getGehalt() {
		return gehalt;
	}
	public void setGehalt(double gehalt) {
		this.gehalt = gehalt;
	}
	//TODO: 2. Implementieren Sie die entsprechende get-Methoden. 
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String vollname() {
		String vollerName =  getVorname() + " " + getName();
		return vollerName;
	}
	 
	 //TODO: 6. Implementieren Sie eine Methode 'vollname', die den vollen Namen (Vor- und Zuname) als string zurückgibt.
}
