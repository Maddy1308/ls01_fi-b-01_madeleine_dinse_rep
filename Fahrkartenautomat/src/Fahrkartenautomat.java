﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {   
       double zuZahlenderBetrag; //Variable: zuZahlenderBetrag, Datentyp: double
       double eingezahlterGesamtbetrag; // Variable: eingezahlterBetrag, Datentyp: double

       zuZahlenderBetrag = fahrkartenbestellungErfassen();
       eingezahlterGesamtbetrag = fahrkartenbezahlen(zuZahlenderBetrag);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein/die Fahrscheine\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
						  "Wir wünschen Ihnen eine gute Fahrt.");
    }
    public static double fahrkartenbestellungErfassen() {
    	double zuZahlenderBetrag = 0; //Variable: zuZahlenderBetrag, Datentyp: double
    	int anzahl_tickets; //Datentyp int, da Anzahl der Tickets eine ganzzahlige und meist kleine Zahl ist
        Scanner tastatur = new Scanner(System.in);
        String[] ticketArt = {"1) Einzelfahrschein Berlin AB", "2) Einzelfahrschein Berlin BC", "3) Einzelfahrschein Berlin ABC", "4) Kurzstrecke", 
        "5) Tageskarte Berlin AB", "6) Tageskarte Berlin BC", "7) Tageskarte Berlin ABC", "8) Kleingruppen-Tageskarte Berlin AB", "9) Kleingruppen-Tageskarte Berlin BC", "10) Kleingruppen-Tageskarte Berlin ABC"};
        double[] ticketPreis = {2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
     
        for(int i = 0; i < ticketArt.length; i++){
            System.out.printf("%s hat den Preis %.2f %n", ticketArt[i], ticketPreis[i]);
        }
    	
    	int fahrkarte = tastatur.nextInt();
    	 while (fahrkarte > 10 || fahrkarte < 1) {
			 System.out.println("Falsche Eingabe. Bitte versuchen Sie es erneut.");
			for(int i = 0; i < ticketArt.length; i++){
				System.out.printf("%s hat den Preis %.2f %n", ticketArt[i], ticketPreis[i]);
			}
	     	   fahrkarte = tastatur.nextInt();
			}
			
    	zuZahlenderBetrag = ticketPreis[fahrkarte - 1];
	       System.out.print("Anzahl der Tickets: ");
	       anzahl_tickets = tastatur.nextInt();

	       while (anzahl_tickets > 10 || anzahl_tickets < 1) {
	    	   System.out.println("Ungültige Eingabe. Bitte geben sie eine valide Ticketanzahl an (1-10)");
	    	   anzahl_tickets = tastatur.nextInt();
	       }
		   zuZahlenderBetrag *= anzahl_tickets;
		return zuZahlenderBetrag;
	}
	
	public static double fahrkartenbezahlen(double zuZahlenderBetrag) {
		double eingezahlterGesamtbetrag = 0.00;
		double eingeworfeneMünze = 0.00;
		Scanner tastatur = new Scanner(System.in);
		
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.println("Noch zu zahlen: " + String.format("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)) + " Euro"); //Addition von zwei variablen
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze; //Additon von eingeworfendeMünze zu eingezahlterGesamtbetrag
           }
		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		 double rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag; //Subtraktion von eingezahlterGesamtbetrag und zuZahlenderBetrag
	       if(rückgabebetrag > 0.00)
	       {
	    	   System.out.println("Der Rückgabebetrag in Höhe von " + String.format("%.2f", rückgabebetrag) + " EURO");
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.00) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.00; //Subtraktion 2.00 von rückgabebetrag
	           }
	           while(rückgabebetrag >= 1.00) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.00; //Subtraktion
	           }
	           while(rückgabebetrag >= 0.50) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.50; //Subtraktion
	           }
	           while(rückgabebetrag >= 0.20) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.20; //Subtraktion
	           }
	           while(rückgabebetrag >= 0.10) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.10; //Subtraktion
	           }
	           while(rückgabebetrag >= 0.04)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05; //Subtraktion
	           }
	       }

	}
	
}